/*
Copyright (C) 1996-1997 Id Software, Inc.

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/
// cl_hud.c -- status bar code

#include "quakedef.h"

/*
//muff - hacked out of SourceForge implementation + modified.
==============

SCR_DrawFPS

==============
*/

void SCR_DrawFPS (void)
{
	extern cvar_t show_fps;
	static double lastframetime;
	double t;
	extern int fps_count;
	static int lastfps;
	int x, y;
	char st[80];

	if (!show_fps.value)
		return;

  t = Sys_FloatTime ();
	if ((t - lastframetime) >= 1.0) {
		lastfps = fps_count;
		fps_count = 0;
		lastframetime = t;
	}
	
	sprintf(st, "%3d FPS", lastfps);

	x = vid.width - strlen(st) * 16 - 16;
	y = 0 ; //vid.height - (sb_lines * (vid.height/240) )- 16;
//	Draw_TileClear(x, y, strlen(st)*16, 16);
	Draw_String(x, y, st);

}

double HUD_Change_time;

/*
===============
HUD_Draw
===============
*/

void HUD_Draw (void)
{    
	if (scr_con_current == vid.height)
		return;		// console is full screen
	
if (!cls.demoplayback)
	{
    //Draw_String (0, 0, "HUD TEST string");
    if (HUD_Change_time > Sys_FloatTime())
      //Draw_Pic (vid.width/2 - 32, vid.height/2 - 32, scr_net);
    return;
  }
}