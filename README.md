# ctrQuake
ctrQuake+ is a free, unofficial patch of ctrQuake for the Nintendo 3DS.

## Installation guide
- If you got this far then you know what you're doing, you probably just want the source code anyways

## Notes
- This version should bypass the need for pop.lmp

## TODO List
- [ ] Better input handling
- [x] Networking
- [ ] Hardware rendering
- [x] Better sound processing ( Thanks to Rinnegatamante )
- [ ] Every other ctrQuake objective
- [x] WAD3 Loading
- [x] HLBSP Loading
- [x] CMD history file loading
- [ ] Alpha textures with '{' and index of 255 (from hlbsp file)

## Credits
Felipe Izzo (MasterFeizz) - Main developer of ctrQuake
Rinnegatamante - Sound enhancements

Naievil - any additional patches that make ctrQuake+ a "+"

dr_mabuse1981/Baker - History file loader

## Thanks to
Id Software - Source code of Quake

Smealum and other developers of libctru

MasterFeizz - porting quake in the first place